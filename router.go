package router

import (
	"net/http"
	"net/url"
)

//
// @todo
// * host filter
// * https filter
//
type Router struct {
	RedirectSlash   bool
	NotFoundHandler http.Handler
	routes          map[string][]*route
}

// Get a new router object to register http handlers.
func NewRouter() *Router {
	return &Router{
		RedirectSlash:   false,
		NotFoundHandler: http.NotFoundHandler(),
		routes:          make(map[string][]*route),
	}
}

// Get param from request - shortcut to http.Request.URL.Query().Get()
func GetParam(req *http.Request, key string) string {
	return req.URL.Query().Get(key)
}

// Our route.
type route struct {
	pattern string
	handler http.Handler
}

// Register handler with pattern for GET requests.
func (self *Router) Get(pattern string, handler http.Handler) {
	self.Register("GET", pattern, handler)
}

// Register handler with pattern for POST requests.
func (self *Router) Post(pattern string, handler http.Handler) {
	self.Register("POST", pattern, handler)
}

// Register handler with pattern for PUT requests.
func (self *Router) Put(pattern string, handler http.Handler) {
	self.Register("PUT", pattern, handler)
}

// Register handler with pattern for DELETE requests.
func (self *Router) Delete(pattern string, handler http.Handler) {
	self.Register("DELETE", pattern, handler)
}

// Register handler with pattern for HEAD requests.
func (self *Router) Head(pattern string, handler http.Handler) {
	self.Register("HEAD", pattern, handler)
}

// Register handler with pattern for OPTIONS requests.
func (self *Router) Options(pattern string, handler http.Handler) {
	self.Register("OPTIONS", pattern, handler)
}

// Register handler with pattern for given method. If RedirectSlash is enabled
// it will also register /foo if pattern /foo/ was passed
func (self *Router) Register(method string, pattern string, handler http.Handler) {

	self.routes[method] = append(self.routes[method], &route{pattern, handler})

	if self.RedirectSlash {
		n := len(pattern)
		if n > 0 && pattern[n-1] == '/' {
			self.Register(method, pattern[:n-1], http.HandlerFunc(customRedirectHandler))
		}
	}
}

func customRedirectHandler(w http.ResponseWriter, req *http.Request) {

	url := req.URL.Path + "/"

	if len(req.URL.Query()) > 0 {
		url = url + "?" + req.URL.RawQuery
	}
	if len(req.URL.Fragment) > 0 {
		url = url + "#" + req.URL.Fragment
	}

	http.Redirect(w, req, url, http.StatusTemporaryRedirect)
}

// Serve requests by matching request url path with registered patterns.
func (self *Router) ServeHTTP(writer http.ResponseWriter, req *http.Request) {
	var handler http.Handler
	routes := self.routes[req.Method]

	for _, r := range routes {
		var params url.Values
		var ok bool

		if params, ok = r.match(req.URL.Path); ok {
			handler = r.handler
			if len(params) > 0 {
				req.URL.RawQuery = url.Values(params).Encode() + "&" + req.URL.RawQuery
			}
			break
		}
	}

	if nil == handler {
		handler = self.NotFoundHandler
	}

	handler.ServeHTTP(writer, req)
}

// Check if route matches for the request.
func (self *route) match(path string) (url.Values, bool) {
	var i, j int
	pattern := self.pattern
	params := make(url.Values)

	for i < len(path) {
		switch {
		case j >= len(pattern):
			if len(pattern) > 0 && pattern != "/" && pattern[len(pattern)-1] == '/' && len(path)-i == 1 {
				return params, true
			}
			return nil, false
		case pattern[j] == '{':
			var key, val string
			key, j = substring(pattern, j+1, '}')
			j++ // eat '}'
			var nc byte = '/'
			if len(pattern) > j {
				nc = pattern[j]
			}
			val, i = substring(path, i, nc)
			params.Add(key, val)
		case path[i] == pattern[j]:
			i++
			j++
		default:
			return nil, false
		}
	}

	if j != len(pattern) {
		return nil, false
	}

	return params, true
}

// Get the substring starting at 'offset' and ending with first hit of
// 'breakChar'. Returns substring and end index.
func substring(str string, offset int, breakChar byte) (substr string, idx int) {
	idx = offset
	for idx < len(str) && str[idx] != breakChar {
		idx++
	}
	return str[offset:idx], idx
}
