package router

import (
	"bitbucket.org/simonhartmann/assert"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"
)

func TestRouterServing(t *testing.T) {

	router := NewRouter()
	router.Get("/hello/{name}", http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		param := GetParam(req, "name")
		assert.AreEqual(param, "world", t)
	}))

	req, err := http.NewRequest("GET", "/hello/world?x=y", nil)
	assert.IsNil(err, t)

	router.ServeHTTP(nil, req)
}

func TestRouterRedirectSlash(t *testing.T) {

	router := NewRouter()
	router.RedirectSlash = true
	router.Get("/hello/{name}/", http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		param := GetParam(req, "name")
		assert.AreEqual(param, "world", t)
	}))

	req, err := http.NewRequest("GET", "/hello/world", nil)
	assert.IsNil(err, t)

	w := httptest.NewRecorder()

	router.ServeHTTP(w, req)
}

func TestRouteMatching(t *testing.T) {

	var params url.Values
	var ok bool

	_, ok = (&route{"/", nil}).match("/")
	assert.IsTrue(ok, t)

	_, ok = (&route{"/", nil}).match("/blabla")
	assert.IsFalse(ok, t)

	params, ok = (&route{"/hello/{name}", nil}).match("/hello/world")
	assert.IsTrue(ok, t)
	assert.AreEqual(params, url.Values{"name": {"world"}}, t)

	params, ok = (&route{"/hello/{name}/banana", nil}).match("/hello/world/banana")
	assert.IsTrue(ok, t)
	assert.AreEqual(params, url.Values{"name": {"world"}}, t)

	params, ok = (&route{"hello/{name}/banana/{id}", nil}).match("hello/world/banana/42")
	assert.IsTrue(ok, t)
	assert.AreEqual(params, url.Values{"name": {"world"}, "id": {"42"}}, t)

	params, ok = (&route{"hello/{name}/banana/{id}", nil}).match("hello/world/banana")
	assert.IsFalse(ok, t)

	params, ok = (&route{"hello/{name}/bananas/", nil}).match("hello/world/bananas/42")
	assert.IsFalse(ok, t)

	params, ok = (&route{"hello/{name}", nil}).match("hello/world/bananas")
	assert.IsFalse(ok, t)

	params, ok = (&route{"hello/{name}.log", nil}).match("hello/world.log")
	assert.IsTrue(ok, t)
	assert.AreEqual(params, url.Values{"name": {"world"}}, t)
}
