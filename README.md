# Simple http router #

Simple router with support for named parameters, e.g.:

## Usage ##

```
#!go

package main

import (
        "bitbucket.org/simonhartmann/router"
        "fmt"
        "net/http"
)

func main() {
        r := router.NewRouter()
        r.Get("/hello/{name}", http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
                param := router.GetParam(req, "name")
                fmt.Fprintf(w, "hello %s!\n", param)
        }))

        http.Handle("/", r)
        err := http.ListenAndServe(":1234", nil)
        if err != nil {
                fmt.Printf("ListenAndServe: %s", err)
        }
}
```
